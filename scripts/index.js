$(document).ready(function(){
    $.ajax({
        url: 'user/user.php?p=get_data',
        success: function (data) {
            data = JSON.parse(data);
            if(!data){
                window.location.href = 'user/templates/login.html';
            }
        }
    });
    
    zoom();
    
    $(window).resize(function(){
       zoom(); 
    });

    function zoom(){
        var htmlHeight = $('html').innerHeight();
        var winHeight = $(window).height();
        var scale = winHeight / htmlHeight;
        
        $('body').css({
            '-ms-transform': 'scale('+ scale +')',
            'transform': 'translateX('+ (scale-1)*50 +'%) translateY('+ (scale-1)*50 +'%) scale('+ scale +')',
            'overflow': 'hidden'
        });  
    }
    
});