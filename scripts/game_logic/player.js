const PLAYER_MOVE_SPEED = 3;
const NUMBER_OF_PLAYERS = 4;

var players = [4];

function playerClass(){
    this.keysHeld = 0;
    this.x = 400, y = 450;
    this.picLoaded = false;
    this.keyPressed_North = false;
    this.keyPressed_East = false;
    this.keyPressed_South = false;
    this.keyPressed_West = false;
    this.keyPressed_Bomb = false;
    this.myName;
    this.bombsHeld = 1;
    this.bombRange = 1;
    this.alive = true;
    this.color = "cyan";
    
    //initialites the control keys 
    this.setupControls = function(upKey, rightKey, downKey, leftKey, bombKey){
        this.controlKeyForNorth = upKey;
        this.controlKeyForEast = rightKey;
        this.controlKeyForSouth = downKey;
        this.controlKeyForWest = leftKey;
        this.controlKeyForBomb = bombKey;
    }

    //initialization of the player object
    this.init = function(whichGraphic, whichName, whichColor){
        this.myBitmap = whichGraphic;
        this.myName = whichName;
        this.color = whichColor;
        this.reset();
    };

    //used also to initialize the player
    this.reset = function(){
        if(this.homeX == undefined){    
            for (eachCol=0; eachCol<ROOM_COLS; eachCol++){
                for (eachRow=0; eachRow<ROOM_ROWS; eachRow++){
                    var tileIndex = TileToIndex(eachCol, eachRow);
                    if(roomGrid[tileIndex]==TILE_PLAYER){
                        this.homeX = eachCol * TILE_W + TILE_W/2;
                        this.homeY = eachRow * TILE_H + TILE_H/2;
                        roomGrid[tileIndex]=TILE_GROUND;
                        break;
                    }
                    if (this.homeX != undefined){break;}
                }
            }
        }
        this.x = this.homeX;
        this.y = this.homeY;
    };
    
    this.move = function(){
        var newX = this.x;
        var newY = this.y;

        if(this.keyPressed_North == true){
            newY += -PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_East == true){
            newX += PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_South == true){
            newY += PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_West == true){
            newX += -PLAYER_MOVE_SPEED;
        }

        var walkIntoTileIndex = getTileIndexAtPixelCoord(newX, newY);
        var walkIntoTileType = TILE_WALL;
        if(walkIntoTileIndex != undefined){
            walkIntoTileType = roomGrid[walkIntoTileIndex]%100;
        }
        
        //calculates the movement of the player
        switch( walkIntoTileType){
            case TILE_GROUND:
                this.x = newX;
                this.y = newY;
                break;

            case TILE_BLAST:
                this.x = newX;
                this.y = newY;
                this.alive = false;
                break;

            case TILE_RANGE_UP:
                this.x = newX;
                this.y = newY;
                this.bombRange++;
                roomGrid[walkIntoTileIndex] = 0;
                break; 

            case TILE_BOMBS_UP:
                this.x = newX;
                this.y = newY;
                this.bombsHeld++;
                roomGrid[walkIntoTileIndex] = 0;
                break; 

            case TILE_BOMB:
                if (walkIntoTileIndex == getTileIndexAtPixelCoord(this.x, this.y)) {
                    this.x = newX;
                    this.y = newY;
                }

            case TILE_WALL:
            default:
                
                break;
        }
    }
    
    this.putBomb = function() {
        if (this.bombsHeld>0 && roomGrid[getTileIndexAtPixelCoord(this.x, this.y)] != TILE_BOMB) {
            var bombNr = activeBombs.length;
            var bombId = "bomb" + bombNr;

            activeBombs.push(new bombClass);
            activeBombs[bombNr].init(this.x, this.y, this.bombRange, this.myName);
        }
    }
    
    this.reduceBombsHeld = function(amount) {
        this.bombsHeld = this.bombsHeld - amount;
    }

    this.draw = function(color){
        drawBall(this.x,this.y,TILE_H/2-5,color);
        //drawCenteredRotatingBitMap(this.myBitmap, this.x, this.y, 0.0);
    }
}

function lastPlayerStanding(){
    var deadPlayerCount = 0; 
    for(player of players) {
        if(player.alive === false) {
            deadPlayerCount++;
        }
    }
    if (deadPlayerCount===(NUMBER_OF_PLAYERS-1)) {
        return true;
    }
}