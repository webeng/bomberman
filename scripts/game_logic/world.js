const TILE_W = 80;
const TILE_H = 80;
const ROOM_COLS = 15;
const ROOM_ROWS = 11;
var trackColor = "blue";
var roomGrid =
    [ 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  1, 1,
      1,  9, 0, 0, 2, 0, 2, 0, 2, 0, 2, 0, 0,  9, 1,
      1,  0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1,  0, 1,
      1,  0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2,  0, 1,
      1,  2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1,  2, 1,
      1,  0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2,  0, 1,
      1,  2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1,  2, 1,
      1,  0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2,  0, 1,
      1,  0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1,  0, 1,
      1,  9, 0, 0, 2, 0, 2, 0, 2, 0, 2, 0, 0,  9, 1,
      1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  1, 1];
const TILE_GROUND = 0;
const TILE_WALL = 1;
const TILE_PLAYER = 9;
const TILE_GOAL = 3;
const TILE_KEY = 4;
const TILE_BOMB = 5;
const TILE_BLAST = 6;
const TILE_BREAKABLE_WALL = 2;
const TILE_BLASTING_WALL = 7;
const TILE_RANGE_UP = 11;
const TILE_BOMBS_UP = 12;


function drawRoom(){
    //canvasContext.clearRect(0, 0, 1200, 800);
    var tileIndex=0;
    var tileLeftEdge=0;
    var tileTopEdge=0;

    for (var eachRow=0; eachRow<ROOM_ROWS; eachRow++) {
        for (var eachCol=0; eachCol<ROOM_COLS; eachCol++) {
            var tileIndex = TileToIndex(eachCol, eachRow); //eachRow*ROOM_COLS + eachCol;
            var agnosticTileValue = roomGrid[tileIndex]%100;
            if (imageTransparency(roomGrid[tileIndex])){
                if (agnosticTileValue === 0 || agnosticTileValue === 5) {
                    drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "green");
                }
            }

            if (agnosticTileValue === 1) {
                drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "black");
            }
            if (agnosticTileValue === 0) {
                drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "green");
            }
            if (agnosticTileValue === 2) {
                drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "brown");
            }
            if (agnosticTileValue === TILE_RANGE_UP) {
                drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "green");
                drawRange(eachCol*TILE_W, eachRow*TILE_H);
            }
            if (agnosticTileValue === TILE_BOMBS_UP) {
                drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W, TILE_H, "green");
                drawBombs(eachCol*TILE_W, eachRow*TILE_H);
            }
            //indicator for dangerous tiles for testing
            //if (Math.floor(roomGrid[tileIndex]/100) > 0) {
              //  drawRectangle(eachCol*TILE_W, eachRow*TILE_H, TILE_W/6, TILE_H/6, "orange");
            //}
            if (roomGrid[tileIndex] === TILE_BLAST) {
                drawRectangle(eachCol*TILE_W+TILE_W/6, eachRow*TILE_H + TILE_H/6, TILE_W*(2/3), TILE_H*(2/3), "red");
            }
        }
    }
}


function isWallThere(trackTileCol, trackTileRow){
    var tileIndex = TileToIndex(trackTileCol, trackTileRow);
    return (roomGrid[tileIndex] == TILE_WALL);
}

function TileToIndex(TileCol, TileRow){
    var tileIndex = TileCol + ROOM_COLS*TileRow;
    return(tileIndex);
}

function imageTransparency(checkTileType){
    return(checkTileType == TILE_GOAL ||
            checkTileType == TILE_BREAKABLE_WALL ||
            checkTileType == TILE_KEY ||
            checkTileType == TILE_BOMB)
}

function getTileIndexAtPixelCoord(X, Y){
    var tileCol = Math.floor(X/TILE_W);
    var tileRow = Math.floor(Y/TILE_H);
    
    if(tileCol < 0 || tileCol >= ROOM_COLS || tileRow < 0 || tileRow >= ROOM_ROWS){
        return undefined;
    }
    
    var tileIndex = TileToIndex(tileCol, tileRow);
    return tileIndex;
}

function getCentreOfTile(x, y) {
    bombX = 0;
    bombY = 0;
    
    centeredX = Math.floor(x/TILE_W)*TILE_W + TILE_W/2;
    centeredY = Math.floor(y/TILE_H)*TILE_H + TILE_H/2;
    return [centeredX, centeredY];
}