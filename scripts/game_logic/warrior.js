const PLAYER_MOVE_SPEED = 1.5;

function warriorClass(){
    this.keysHeld = 0;
    this.x = 400, y = 450;
    this.picLoaded = false;
    this.keyPressed_North = false;
    this.keyPressed_East = false;
    this.keyPressed_South = false;
    this.keyPressed_West = false;
    
    this.setupControls = function(upKey, rightKey, downKey, leftKey){
        this.controlKeyForNorth = upKey;
        this.controlKeyForEast = rightKey;
        this.controlKeyForSouth = downKey;
        this.controlKeyForWest = leftKey;
    }

    this.init = function(whichGraphic, whichName){
        this.myBitmap = whichGraphic;
        this.myName = whichName;
        this.reset();
    };

    this.reset = function(){
        if(this.homeX == undefined){    
            for (eachCol=0; eachCol<ROOM_COLS; eachCol++){
                for (eachRow=0; eachRow<ROOM_ROWS; eachRow++){
                    var tileIndex = TileToIndex(eachCol, eachRow);
                    if(roomGrid[tileIndex]==TILE_PLAYER){
                        this.homeX = eachCol * TILE_W + TILE_W/2;
                        this.homeY = eachRow * TILE_H + TILE_H/2;
                        roomGrid[tileIndex]=TILE_GROUND;
                        break;
                    }
                    if (this.homeX != undefined){break;}
                }
            }
        }
        this.x = this.homeX;
        this.y = this.homeY;
    };
    
    this.move = function(){
        var newX = this.x;
        var newY = this.y;

        if(this.keyPressed_North == true){
            newY += -PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_East == true){
            newX += PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_South == true){
            newY += PLAYER_MOVE_SPEED;
        }
        if(this.keyPressed_West == true){
            newX += -PLAYER_MOVE_SPEED;
        }

        var walkIntoTileIndex = getTileIndexAtPixelCoord(newX, newY);
        var walkIntoTileType = TILE_WALL;
        if(walkIntoTileIndex != undefined){
            walkIntoTileType = roomGrid[walkIntoTileIndex];
        }
        
        switch( walkIntoTileType){
            case TILE_GROUND:
                this.x = newX;
                this.y = newY;
                break;
            case TILE_KEY:
                this.keysHeld += 1;
                this.x = newX;
                this.y = newY;
                roomGrid[walkIntoTileIndex] = TILE_GROUND;
                document.getElementById("debugText").innerHTML = "You are currently holding " + this.keysHeld + " keys.";
                break;
            case TILE_DOOR:
                if( this.keysHeld>=1){
                    this.keysHeld -= 1;
                    this.x = newX;
                    this.y = newY;
                    roomGrid[walkIntoTileIndex] = TILE_GROUND;
                    document.getElementById("debugText").innerHTML = "You are currently holding " + this.keysHeld + " keys.";
                } else {
                    document.getElementById("debugText").innerHTML = "You need a key to open this door. You are currently holding " + this.keysHeld + " keys.";
                }
                break;
            case TILE_GOAL:
                document.getElementById("debugText").innerHTML = "GZ, " + this.myName + " won the race!! GZ";
                this.reset();
                break;
            case TILE_WALL:
            default:
                
                break;
        }
        /*if (walkIntoTileName == TILE_GROUND){
            this.x = newX;
            this.y = newY;
        } else if(walkIntoTileName == TILE_GOAL){
            document.getElementById("debugText").innerHTML = "GZ, " + this.myName + " won the race!! GZ";
            this.reset();
        }*/
    }
    
    this.draw = function(){
        drawCenteredRotatingBitMap(this.myBitmap, this.x, this.y, 0.0);
    }

    function getTileIndexAtPixelCoord(X, Y){
    var tileCol = Math.floor(X/TILE_W);
    var tileRow = Math.floor(Y/TILE_H);
    
    if(tileCol < 0 || tileCol >= ROOM_COLS || tileRow < 0 || tileRow >= ROOM_ROWS){
        return undefined;
    }
    
    var tileIndex = TileToIndex(tileCol, tileRow);
    return tileIndex;
    }
}