var BackGroundcolour = "black";
const FRAMESPERSECOND = 60;
var victoryScreen = false;
var p1 = new playerClass();
var p2 = new playerClass();
var p3 = new playerClass();
var p4 = new playerClass();
var haltGame = false;

players[0] = p1;
players[1] = p2;
players[2] = p3;
players[3] = p4;


window.onload = function(){
    canvas = document.getElementById("gameCanvas");
    canvasContext = canvas.getContext("2d");
    loadImages();
    haltGame = false;
};

function loadingDone(){
    inputInit();
    p1.init(playerPic, "p1", "cyan");
    p2.init(playerPic, "p2", "pink");
    p3.init(playerPic, "p3", "purple");
    p4.init(playerPic, "p4", "fuchsia");
    
    setInterval (function(){
        if(haltGame!==true) {
            moveEverything();
            drawEverything();
            checkFatalState();
        }
        }, 1000/FRAMESPERSECOND);
}

function drawEverything(){
    drawRoom();
    bombsDraw();
    for(player of players) {
        if(player.alive === true) {
            player.draw(player.color);
        }
    }    
}

function drawVictoryMessage(){
    canvasContext.textAlign="center";
    canvasContext.fillStyle = "white";
    canvasContext.fillText("GZ, you won! Click for the next round!", canvas.width/2, canvas.height/2);
}

function moveEverything(){
    bombsTick();
    for(player of players) {
        if(player.alive) {
            player.move();
        }
    }
}

function checkFatalState() {
    if(lastPlayerStanding()){
        var winMessage;
        haltGame = true;
        if(p1.alive){
            saveGame(10);
        }
        winMessage = "Congratulations, you won!";
        canvasContext.font="90px Verdana";
        canvasContext.fillText(winMessage,50,90); 
    }
}

function saveGame(points){
    $.ajax({
       url: 'user/user.php?p=save_game',
       method: 'POST',
       data: {'points': points},
       success: function(data){
           data = JSON.parse(data);
           if(!data){
                var winMessage = 'The Game could not be saved!\nYou are probaply not logged in.';
                canvasContext.font="90px Verdana";
                canvasContext.fillText(winMessage,50,90);
           }else{
                winMessage = 'Congratulations, you won!';
                canvasContext.font="90px Verdana";
                canvasContext.fillText(winMessage,50,90);
           }
       }
    });
}