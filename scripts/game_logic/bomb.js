var activeBombs = [];
var blastTiles = [];
var blastCounter = 0;

function bombsDraw() {
    if (activeBombs.length == 0) {
        return false;
    }
    for (var i=0; i<activeBombs.length; i++) {
        activeBombs[i].draw();
    }
}

function bombsTick() {
    for (var i=0; i<activeBombs.length; i++) {
        var currentBomb = activeBombs[i];
        currentBomb.detonationTime--;
        
        if (currentBomb.detonationTime<=0) {
            var playerToGetBomb = currentBomb.player;
            for (var p=0; p<players.length; p++) {
                if(playerToGetBomb == players[p].myName) {
                    players[p].reduceBombsHeld(-1);
                }
            }
            roomGrid[getTileIndexAtPixelCoord(currentBomb.x, currentBomb.y)] = 0;
            updateDangerGrid();
            /* calculateDetonation(currentBomb.x, currentBomb.y); */
            activeBombs.splice(currentBomb, 1);
        }

        updateDangerGrid();
    }

    for (var b=blastTiles.length-1; b>=0; b--) {
        var currentBlast = blastTiles[b];
        currentBlast.reduceLifeTime();
    }

    for (var b=blastTiles.length-1; b>=0; b--) {
        var currentBlast = blastTiles[b];
        if (currentBlast.lifeTime<=0) {
            blastTiles.splice(currentBlast, 1);
        }
    }
    return false;
}

//use bool finalCheck to mark the detonation tiles, they will get detonated in calculateDetonation
function updateDangerGrid() {
    for (var i=0; i<activeBombs.length; i++) {
        var range = activeBombs[i].range;
        var bombIndex = getTileIndexAtPixelCoord(activeBombs[i].x, activeBombs[i].y);
        var checkNorth = true;
        var checkEast = true;
        var checkSouth = true;
        var checkWest = true;
        var finalCheck = false;

        if (activeBombs[i].detonationTime<=0) {
            finalCheck = true;
            generateExplosionTile(bombIndex);
        }

        for (var currentRadius = 1; currentRadius<=range; currentRadius++) {
            //north
            var northIndex = bombIndex-ROOM_COLS*currentRadius;
            if (checkNorth && checkForExplosiveTile(northIndex)) {
                if(checkForNotYetDangerous(northIndex)) {
                    roomGrid[northIndex] += 100;
                }
                if (checkExplosionBlocker(northIndex)) {
                    checkNorth = false; 
                }
                if (finalCheck) {
                    if (checkForNoBomb(northIndex)) {
                        generateExplosionTile(northIndex);
                    } else {
                        explodeContagiously(northIndex);
                    }
                }
                
            } else {
                checkNorth = false;
            }
            //east
            var eastIndex = bombIndex + currentRadius;
            if (checkEast && checkForExplosiveTile(eastIndex)) {
                if(checkForNotYetDangerous(eastIndex)) {
                    roomGrid[eastIndex] += 100;
                }
                if (checkExplosionBlocker(eastIndex)) {
                    checkEast = false; 
                }
                if(finalCheck) {
                    if (checkForNoBomb(eastIndex)) {
                        generateExplosionTile(eastIndex);
                    } else {
                        explodeContagiously(eastIndex);
                    }
                }
            } else {
                checkEast = false;
            }
            //south
            var southIndex = bombIndex+ROOM_COLS*currentRadius;
            if (checkSouth && checkForExplosiveTile(southIndex)) {
                if(checkForNotYetDangerous(southIndex)) {
                    roomGrid[southIndex] += 100;
                }
                if (checkExplosionBlocker(southIndex)) {
                    checkSouth = false; 
                }
                if(finalCheck) {
                    if (checkForNoBomb(southIndex)) {
                        generateExplosionTile(southIndex);
                    } else {
                        explodeContagiously(southIndex);
                    }
                }
            } else {
                checkSouth = false;
            }
            //west
            var westIndex = bombIndex - currentRadius;
            if (checkWest && checkForExplosiveTile(westIndex)) {
                if(checkForNotYetDangerous(westIndex)) {
                    roomGrid[westIndex] += 100;
                }
                if (checkExplosionBlocker(westIndex)) {
                    checkWest = false; 
                }
                if(finalCheck) {
                    if (checkForNoBomb(westIndex)) {
                        generateExplosionTile(westIndex);
                    } else {
                        explodeContagiously(westIndex);
                    }
                }
            } else {
                checkWest = false;
            }
        }
    }
}

function checkForExplosiveTile(indexToCheck) {
    if (roomGrid[indexToCheck]%100 == 1) {
        return false;
    }
    return true;
}

function checkForNotYetDangerous(indexToCheck) {
    if (Math.floor(roomGrid[indexToCheck]/100) > 0 || roomGrid[indexToCheck] == TILE_BLAST) {
        return false; 
    }
    return true;
}

function checkExplosionBlocker(indexToCheck) {
    if (roomGrid[indexToCheck]%100 == 0 || roomGrid[indexToCheck]%100 == TILE_BLAST) {
        return false;
    }
    return true;
}

function checkForNoBomb(indexToCheck) {
    if (roomGrid[indexToCheck]%100 == 5) {
        return false;
    }
    return true;
}

function explodeContagiously(index) {
    for (var i=0; i<activeBombs.length; i++) {
        if(getTileIndexAtPixelCoord(activeBombs[i].x, activeBombs[i].y) == index) {
            activeBombs[i].detonationTime = 1;
        }
    }
}

function generateExplosionTile(index){
    var blastNr = blastTiles.length;
    var blastId = "blast" + blastNr;
    var isBlastedWall = false;

    if(roomGrid[index]%100 === 2) {
        isBlastedWall = true;
    }
    blastTiles.push(new BlastTileClass);
    blastTiles[blastNr].init(index, this.blastId, isBlastedWall);
}

function bombClass(){
    this.detonationTime = FRAMESPERSECOND*3;
    this.x = 400;
    this.y = 450;
    this.picLoaded = false;
    this.range = 2;
    this.player;
   
    //initialization of the player object
    this.init = function(whichX, whichY, whichRange, whichPlayer){
        this.player = whichPlayer;
        this.range = whichRange;
        //this.detonationTime = whichTime; //can add parameter for detonation time later
        this.x = getCentreOfTile(whichX, whichY)[0];
        this.y = getCentreOfTile(whichX, whichY)[1];
        roomGrid[getTileIndexAtPixelCoord(whichX, whichY)] = 5;
        for (var p=0; p<players.length; p++) {
            if (whichPlayer == players[p].myName) {
                players[p].reduceBombsHeld(1);
            }
        }
    };

    this.setDetonationTime = function(t) {
        this.detonationTime = t;
    }

    this.draw = function(){
        drawBall(this.x,this.y,TILE_H/2-5,"grey");
        //drawCenteredRotatingBitMap(this.myBitmap, this.x, this.y, 0.0);
    };
}

function BlastTileClass(){
    this.tileIndex = -1;
    this.lifeTime = FRAMESPERSECOND;
    this.id;
    this.isBlastedWall = false;

    this.init = function (index, blastId, isBlastedWall) {
        this.tileIndex = index;
        roomGrid[index] = TILE_BLAST;
        this.id = blastId;
        this.isBlastedWall = isBlastedWall;
    }

    this.reduceLifeTime = function() {
        this.lifeTime--;
         if (this.lifeTime <= 0) {
            if (this.isBlastedWall) {
                generatePowerUp(this.tileIndex);
            } else {
                roomGrid[this.tileIndex] = 0;
            }
            //blastTiles.splice(blastTiles[blastInsance], 1);
        } 
    }
}