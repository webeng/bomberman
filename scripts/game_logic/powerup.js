function generatePowerUp(index) {
    var randomChance = Math.random();
    if (randomChance<=0.25 || randomChance>0.75) {
        roomGrid[index] = 0;
    }
    if (randomChance >0.25 && randomChance<=0.5) {
        roomGrid[index] = 11;
    }
    if (randomChance >0.5 && randomChance<=0.75) {
        roomGrid[index] = 12;
    }
}