const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;
const KEY_LEFT_ARROW = 37;
const KEY_SPACE_BAR = 32;

const KEY_W = 87;
const KEY_S = 83;
const KEY_A = 65;
const KEY_D = 68;
const KEY_E = 69;

const KEY_I = 73;
const KEY_J = 74;
const KEY_K = 75;
const KEY_L = 76;
const KEY_O = 79;

const KEY_T = 84;
const KEY_F = 70;
const KEY_G = 71;
const KEY_H = 72;
const KEY_Z = 90;

function inputInit(){
    document.addEventListener("keydown", keyPressed);
    document.addEventListener("keyup", keyReleased);
    p1.setupControls(KEY_UP_ARROW, KEY_RIGHT_ARROW, KEY_DOWN_ARROW, KEY_LEFT_ARROW, KEY_SPACE_BAR);
    p3.setupControls(KEY_W, KEY_D, KEY_S, KEY_A, KEY_E);
    p4.setupControls(KEY_I, KEY_L, KEY_K, KEY_J, KEY_O);
    p2.setupControls(KEY_T, KEY_H, KEY_G, KEY_F, KEY_Z);
    
}

function keyPressed(evt){
    checkBombPlacement(evt.keyCode, p1);
    checkBombPlacement(evt.keyCode, p2);
    checkBombPlacement(evt.keyCode, p3);
    checkBombPlacement(evt.keyCode, p4);
    SetKeyHoldState(evt.keyCode, p1, true);
    SetKeyHoldState(evt.keyCode, p2, true);
    SetKeyHoldState(evt.keyCode, p3, true);
    SetKeyHoldState(evt.keyCode, p4, true);
    evt.preventDefault();
    }
    


function keyReleased(evt){
    SetKeyHoldState(evt.keyCode, p1, false);
    SetKeyHoldState(evt.keyCode, p2, false);
    SetKeyHoldState(evt.keyCode, p3, false);
    SetKeyHoldState(evt.keyCode, p4, false);
}

function SetKeyHoldState(thisKey, thisPlayer, setTo){
    if(thisKey == thisPlayer.controlKeyForNorth){
        thisPlayer.keyPressed_North = setTo;
    }
    if(thisKey == thisPlayer.controlKeyForEast){
        thisPlayer.keyPressed_East = setTo;
    }
    if(thisKey == thisPlayer.controlKeyForSouth){
        thisPlayer.keyPressed_South = setTo;
    }
    if(thisKey == thisPlayer.controlKeyForWest){
        thisPlayer.keyPressed_West = setTo;
    }
}

function checkBombPlacement(thisKey, thisPlayer) {
    if(thisKey == thisPlayer.controlKeyForBomb) {
        thisPlayer.putBomb();
    }
}
