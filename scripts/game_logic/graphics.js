var playerPic = document.createElement("img");
var tilePics=[   ];
var picsToLoad = 2;
//
var SQUARE_SIZE = 1000/11;

function countLoadedImageAndLaunchIfReady(){
    picsToLoad-=1;
    if (picsToLoad == 0){
        loadingDone();
    }
}

function loadImages(){
    
     var imageList = [
        /* {trackType: TILE_GROUND, path:"world_ground.png"},
        {trackType: TILE_WALL, path:"world_wall.png"}, */
        {pic: playerPic, path:"bomberman.png"},
    ];
    
    picsToLoad= imageList.length
    
    for(i=0; i<imageList.length; i++){
        if(imageList[i].trackType != undefined){
            loadImageForTileCode(imageList[i].trackType, imageList[i].path);
        } else {
            beginLoadingImage(imageList[i].pic, imageList[i].path);
        }
    }
}

function beginLoadingImage(pic, path){
    pic.onload=countLoadedImageAndLaunchIfReady;
    pic.src="images/"+path;
}

function loadImageForTileCode(trackCode, fileName){
    tilePics[trackCode] = document.createElement("img");
    beginLoadingImage(tilePics[trackCode], fileName);
}

function drawWall (wallX, wallY) {
    canvasContext.drawImage(WallPic, wallX, wallY);
}

function drawCenteredRotatingBitMap(pic, picX, picY, angle){
    canvasContext.save();
    canvasContext.translate(picX, picY);
    canvasContext.rotate(angle);
    canvasContext.drawImage(pic, -pic.width/2, -pic.height/2);
    canvasContext.restore();
}

//provisorische graphiken
function drawBall(ballX,ballY,ballRadius,ballColour){
    canvasContext.fillStyle = ballColour;
    canvasContext.beginPath();
    canvasContext.arc(ballX, ballY, ballRadius, 0, 2*Math.PI, true);
    canvasContext.fill();
}

function drawRectangle(X, Y, width, height, colour){
    canvasContext.fillStyle = colour;
    canvasContext.fillRect(X, Y, width, height);
}

function drawBombs(X, Y){
    var icon = new Image;
    icon.src = 'images/bombs_up.png';
    canvasContext.drawImage(icon, X, Y, 80, 80);
}

function drawRange(X, Y){
    var icon = new Image;
    icon.src = 'images/range_up.png';
    canvasContext.drawImage(icon, X, Y, 80, 80);
}