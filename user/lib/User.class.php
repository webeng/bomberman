<?php

require_once '../lib/Database.class.php';

/**
 * Description of User
 *
 * @author ar319002
 */
class User {

    private $id;
    private $name;
    private $email;
    private $password;
    private $description;
    private $points;

//erstellt einen neuen User
//alles bis auf $email sind mit defualt wert besetzt, sodass auch ein User Objekt so erzeugt werden kann
    public function __construct(String $name, String $email="test", String $password="none", String $desc = "Hey there! I am playing Bomberman."
            . "", int $points = 0) {
        if ($name === "" || $email === "" || $password === "" || $points < 0) {
            throw new Exception("unzulässige Parameter!");
        }
        if (self::userExists($name)) {
            $userInfo = self::checkForInfos($name);
            $this->name = $name;
            $this->email = $userInfo[0]['email'];
            $this->password = $userInfo[0]['password'];
            $this->description = $userInfo[0]['description'];
            $this->points = $userInfo[0]['points'];
            $this->id = $userInfo[0]['id'];
        } else {
            $this->name = $name;
            $this->email = $email;
            $this->password = $password;
            $this->description = $desc;
            $this->points = $points;
        }
    }

    //wenn der Nutzende schon existiert wird true zurückgegeben
    public static function userExists(String $name) :bool{
        $dbh = Database::getInstance();
        $sql = 'SELECT * FROM users WHERE name= :name';
        $sth = $dbh->prepare($sql);
        $sth->bindParam('name', $name);
        $sth->execute();
        $ret = $sth->fetchAll();
        return !empty($ret);
    }
    
    //ueberprueft, ob der user schon in der Datenbank abgespeichert ist
    public function isSaved() :bool{
        $dbh = Database::getInstance();
        $sql = 'SELECT * FROM users WHERE name= :name';
        $sth = $dbh->prepare($sql);
        $sth->bindParam('name', $this->name);
        $sth->execute();
        $ret = $sth->fetchAll();
        return !empty($ret);
    }

    private static function checkForInfos(String $name) {
        $dbh = Database::getInstance();
        $sql = 'SELECT * FROM users WHERE name= :name';
        $sth = $dbh->prepare($sql);
        $sth->bindParam('name', $name);
        $sth->execute();
        return $sth->fetchAll();
    }

    public function getInfos() {
        return array(
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'description' => $this->description,
            'points' => $this->points
        );
    }
    
    //Uebergibt die 10 besten user als Array
    public static function getAll(){
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("SELECT name, points FROM users ORDER BY points DESC limit 10");
        $sth->execute();
        return $sth->fetchAll();
    }
    
    public function getID(){
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("SELECT id FROM users WHERE name = :name");
        $sth->bindParam('name', $this->name);
        $sth->execute();
        return (int)$sth->fetchAll()[0]['id'];
    }
    
    public function saveGame(int $points){
        $id = $this->getID();
        $time = date('Y-m-d H:i:s', time());
        
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("
                INSERT INTO played_games (user_id, points_earned, time) VALUES (:id, :points, :time);
                UPDATE users SET points = points + :points WHERE id = :id;
                ");
        $sth->bindParam('id', $id);
        $sth->bindParam('points', $points);
        $sth->bindParam('time', $time);
        $ret = $sth->execute();
        $this->points += $points;
        return $ret;
    }

//speichert den User in der Datenbank ab
    public function saveUser():bool {
        if(!self::userExists($this->name)){
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("INSERT INTO users (name, email, password, description, points) VALUES (:name, :email, :passw, :des, :p)");
        $sth->bindParam('name', $this->name);
        $sth->bindParam('email', $this->email);
        $sth->bindParam('passw', $this->password);
        $sth->bindParam('des', $this->description);
        $sth->bindParam('p', $this->points);
        $ret = $sth->execute();
        
        return $ret;
        } else {
            return false;
        }
    }
    
    public function setDescription(string $desc){
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("UPDATE users SET description = :desc WHERE name = :name");
        $sth->bindValue('desc', $desc);
        $sth->bindValue('name', $this->name);
        $sth->execute();
        $this->description = $desc;
    }

    //gibt die 10 besten Einträge des Nutzers wieder
    public function getRecords(): array {
        $dbh = Database::getInstance();
        $sth = $dbh->prepare("SELECT points FROM users WHERE id = :uid ORDER BY points DESC limit 10");
        $sth->bindValue('uid', $this->id);
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * @return String Name des Users
     */
    public function toString() {
        return $this->name;
    }

    public function get(String $param) {
        if (isset($this->$param)) {
            return $this->$param;
        }
        return false;
    }

}
