<?php
if(isset($_POST['abmelden'])){
    session_unset();
}

if (!isset($_SESSION['user_id'])) {
    if ($_GET['page'] == 'register') {
        $smarty->assign('title', 'Registrieren');
        $smarty->assign('mainContent', 'register');
        $smarty->assign('css', 'login');
    } else {
        if (isset($_SESSION['login_username'])) {
            $smarty->assign('username', $_SESSION['login_username']);
        } else {
            $smarty->assign('username', 'Benutzername');
        }
        if (isset($_SESSION['login_password'])) {
            $smarty->assign('password', $_SESSION['login_password']);
        } else {
            $smarty->assign('password', 'Passwort');
        }
        $smarty->assign('page', $_GET['page']);

        $smarty->assign('title', 'Login');
        $smarty->assign('mainContent', 'login');
        $smarty->assign('css', 'login');
        session_unset();
    }
} else {
    if (user::isAdmin($_SESSION['user_id'])) {
        include 'cart.php';
    } else {
        if (!isset($dbh)) {
            $dbh = Database::getInstance();
        }
        $ids = Article::getIDs();
        $articles = array();
        foreach ($ids as $id) {
            $article = new Article($id['article_id']);
            $articles[$id['article_id']] = $article->getArticle();
        }
        $colheight = array(0, 0, 0, 0, 0, 0);
        foreach ($articles as $key => $article) {
            $min = 0;
            for ($i = 1; $i <= 5; ++$i) {
                if ($colheight[$i] < $colheight[$min]) {
                    $min = $i;
                }
            }
            $src = $article['src'];
            $size = getimagesize($src);
            $colheight[$min] += $size['1'];
            $articles[$key]['column'] = $min;
        }
        if (isset($added)) {
            $articles['added'] = $added;
        }
        $smarty->assign('articles', $articles);

        $stmt = $dbh->prepare("SELECT * FROM options");
        $stmt->execute();
        $options = $stmt->fetchAll();
        $smarty->assign('options', $options);

        $smarty->assign('title', 'Shop');
        $smarty->assign('mainContent', "shop");
        $smarty->assign('css', "shop");
        $smarty->assign('js', "shop");
        $smarty->assign('tocart', "tocart");
    }
}
?>

