<?php

session_start();

require dirname(__FILE__) . '/lib/User.class.php';

$name;
$mail;
$pw;
if ($_GET['p'] === 'sign_off'){
    $_SESSION = array();
} else if ($_GET['p'] === 'register_post') {
    $_SESSION = array();
    $name = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $mail = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL);
    $pw = password_hash(filter_input(INPUT_POST, 'pw', FILTER_UNSAFE_RAW), PASSWORD_BCRYPT);

    if (isset($name) && isset($pw) && isset($mail)) {
        if (User::userExists($name)) {
            echo json_encode(FALSE);
        } else {
            $user = new User($name, $mail, $pw);
            $success = $user->saveUser();
            $_SESSION['user'] = serialize($user);
            echo json_encode($user->toString());
        }
    }
} else if ($_GET['p'] === 'login_post') {
    $_SESSION = array();
    $name = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $pw = filter_input(INPUT_POST, 'pw', FILTER_UNSAFE_RAW);

    if (isset($name) && isset($pw)) {
        $user = new User($name);
        if (password_verify($pw, $user->get('password'))) {
            $_SESSION['user'] = serialize($user);
            echo json_encode($user->getInfos());
        } else {
            echo json_encode(FALSE);
        }
    } else {
        echo json_encode(FALSE);
    }
} else if (isset($_SESSION['user'])) {
    if ($_GET['p'] === 'get_data') {
        $user = unserialize($_SESSION['user']);

        if (isset($user) && $user->isSaved()) {
            $self = $user->getInfos();
            $all = User::getAll();
            echo json_encode(array('self' => $self, 'all' => $all));
        } else {
            echo json_encode(false);
        }
    } else if ($_GET['p'] === 'post_desc' && isset($_POST['desc'])) {
        $user = unserialize($_SESSION['user']);

        $desc = filter_input(INPUT_POST, 'desc', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if (isset($user)  && $user->isSaved()) {
            $user->setDescription($desc);
            $_SESSION['user'] = serialize($user);
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    } else if($_GET['p'] === 'save_game' && isset($_POST['points'])){
        $user = unserialize($_SESSION['user']);
        
        $points = filter_input(INPUT_POST, 'points', FILTER_SANITIZE_NUMBER_INT);
        
        if(isset($user) && $user->isSaved() && $user->saveGame((int)$points)){
            $_SESSION['user'] = serialize($user);
            echo json_encode(TRUE);
        }else{
            echo json_encode(FALSE);
        }
    }else{
        echo json_encode(FALSE);
    }
}else{
    echo json_encode(FALSE);
}