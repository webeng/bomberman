//Hier ist der Template Teil in einer Seperaten Datei
            var source = $('#adress-template').html();
            var template = Handlebars.compile(source);
            var data = {city: 'Hamburg',
                        number: '50169',
                        street: 'Nachtigallenweg'};
            var html = template(data);
            $('#content-placeholder').html(html);

Handlebars.registerHelper('agree_button', function() {
  var emotion = Handlebars.escapeExpression(this.emotion),
      name = Handlebars.escapeExpression(this.name);

  return new Handlebars.SafeString(
    "<button>Ich " + emotion + " " + name + "</button>"
  );
});

var src = $('#adress').html();
var tpl = Handlebars.compile(src);
var context = {
  items: [
    {name: "Handlebars", emotion: "mag"},
    {name: "Mustache", emotion: "habe noch nie gearbeitet mit"},
    {name: "Smarty", emotion: "preferiere"}
  ]
};
var htm = tpl(context);
$('#second-place').html(htm);

