$(document).ready(function () {
    $('#Login').click(function (event) {
        event.preventDefault(event);
        postLogin();
    });

    $('#Register').click(function (event) {
        event.preventDefault(event);
        postRegister();
    });

    function postRegister() {
        var user = $('#register_user').val();
        var mail = $('#mail').val();
        var pw = $('#pass').val();
        if (user.length === 0) {
            $('#user-label').append(' <label id="username-error" style="color: red">Bitte ausfüllen</label>');
        } else if (pw.length < 8) {
            $('#pw-label').append(' <label id="password-error" style="color: red">Bitte min. 8 Zeichen</label>');
        } else if (mail.length === 0) {
            $('#mail-label').append(' <label id="mail-error" style="color: red">Bitte ausfüllen</label>');
        } else {
            $.ajax({
                method: 'POST',
                url: '../user.php?p=register_post',
                data: {'user': user,
                    'mail': mail,
                    'pw': pw},
                success: function (data) {
                    data = JSON.parse(data);
                    if (data) {
                        loadProfile();
                    } else {
                        alert("Fehler: " + data);
                    }
                }
            });
        }
    }

    function postLogin() {
        var user = $('#username').val();
        var pw = $('#pw').val();
        if (user.length === 0) {
            $('.username-label').append(' <label id="username-error" style="color: red">Bitte ausfüllen</label>');
        } else if (pw.length < 8) {
            $('.password-label').append(' <label id="password-error" style="color: red">Bitte min. 8 Zeichen</label>');
        } else {
            $.ajax({
                method: 'POST',
                url: '../user.php?p=login_post',
                data: {'user': user,
                    'pw': pw},
                success: function (data) {
                    data = JSON.parse(data);
                    if (data) {
//                            alert('Hallo '+data['name']);
                        loadProfile();
                    } else {
                        alert('Falsche Anmeldeinformationen');
                    }
                }
            });
        }
    }

    function loadProfile() {
//            $.ajax({
//                url: '../templates/main.html',
//                dataType: 'text',
//                success: function(data) {
//                    $('#main').html(data);
//                } 
//            });
        window.location.href = '../templates/main.html';
    }

});


$(document).ready(function () {
    var modal = document.getElementById('id01');
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
});
function visible() {
    var x = document.getElementById("pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
;
function breaking(stil) {
    document.getElementById('id01').style.display = stil;
}
;
 