$(document).ready(function () {
    $('#content-placeholder').on('click', '#submit-description', function () {
        postDesc();
    });
    
    $('#content-placeholder').on('click', '#sign-off', function () {
        signOff();
    });

    $.ajax({
        url: '../user.php?p=get_data',
        success: function (data) {
            data = JSON.parse(data);
            if (data) {
                handleData(data);
            } else {
                window.location.href = '../templates/login.html';
            }
        }
    });


    function handleData(data) {
        var source = $('#content').html();
        var template = Handlebars.compile(source);
        $('#content-placeholder').html(template({
            self: data['self'],
            users: data['all'],
            top_size: data['all'].length
        }));
    }

    function postDesc() {
        var desc = $('#desc').val()
        $.ajax({
            method: 'POST',
            url: '../user.php?p=post_desc',
            data: {'desc': desc},
            success: function (data) {
                data = JSON.parse(data);
                var success = $('span#success')
                if (data) {
                    success.html('&#10004');
                    success.css('color', 'lightgreen');
                } else {
                    success.html('&#10006');
                    success.css('color', 'red');
                }
            }
        });
    }
    
    function signOff(){
        $.ajax({
            url: '../user.php?p=sign_off',
        }).done(function(){
            window.location.href = '../templates/login.html';
        });
    }
});

function startGame() {
    window.location.href = '../../index.html';
}
