<?php
require_once '../lib/Database.class.php';
class Game {
    private $points_earned;
    private $time;
    private $userid = 1;
    
    //bool true false, statt points earned
    public function __construct(int $points_earned, String $userid, String $endtime){
        $dbh = Database::getInstance();
        $sql = "INSERT INTO played_games Values (':id', ':o', ':point', ':t')";
        $pdo = $dbh->prepare($sql);
        $pdo->bindValue('o', $userid);
        $pdo->bindValue('point', $points_earned);
        $pdo->bindValue('t', $endtime);
        $this->points_earned = $points_earned;
        $this->time = $endtime;
        $this->userid = $userid;
        $pdo->execute();
}

    public function toString(){
        return $this->userid;
    }
}
