<?php

/**
 * Singelton Database Object
 *
 * @author Annina Röllinghoff
 */
class Database {

    private static $instance = null;

    public static function getInstance() {
        if (!isset(self::$instance)) { //if no instance, then create one
            try {
                $host = "localhost";
                $user = "root";
                $passwort = "";
                $dbname = "bomberman";
                self::$instance = new PDO("mysql:host=$host; dbname=$dbname; charset=UTF8", $user, $passwort);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
        }
//set names... ->UTF8
        return self::$instance;
    }

}
